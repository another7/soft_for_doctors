from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets, QtGui
import sys
import pandas as pd
from datetime import datetime
from dateutil import relativedelta


df = pd.read_excel("C:\Python/softdoc/tablica.xlsx")


def application():

    # Номер нового пациента
    Number = QLabel(window)
    Number.setText(f"Номер пациента:{len(df)+1}")
    Number.move(5,0)
    Number.adjustSize()

    # Фамилия имя отчество
    FIO = QLabel(window)
    FIO.setText("ФИО")
    FIO.move(5,20)
    FIO.adjustSize()

    FIO_line = QLineEdit(window)
    FIO_line.setText("")
    FIO_line.move(0,35)
    FIO_line.resize(300,30)# функция отвечающая за длину и ширину поля ввода

    # Дата рождения 
    date_birth = QLabel(window)
    date_birth.setText("Дата рождения\nпример ввода: 25.07.1900")
    date_birth.move(5,80)
    date_birth.adjustSize()

    date_birth_line = QLineEdit(window)
    date_birth_line.move(0,110)

    #пол
    gender = QLabel(window)
    gender.setText("Пол")
    gender.move(5,150)
    gender.adjustSize()

    gender_list = QComboBox(window)
    gender_list.addItem("M")
    gender_list.addItem("Ж")
    gender_list.move(0, 165)

    # беременность
    pregnancy = QLabel(window)
    pregnancy_list = QComboBox(window)
    pregnancy_list.addItem("Нет")
    pregnancy_list.addItem("Да")
    pregnancy_list.move(130, 165)
    pregnancy_list.hide() # hide combo box

    # срок беременности 
    gestational_age = QLabel(window)
    gestational_age.setText("Введите срок беременности  в неделях")
    gestational_age.move(260,150)
    gestational_age.adjustSize()
    gestational_age.hide()

    gestational_age_line = QLineEdit(window)
    gestational_age_line.setText(" ")
    gestational_age_line.move(260,165)
    gestational_age_line.hide()

    def onActivated_gender_list():
        if str(gender_list.currentText()) == "Ж":
            pregnancy.setText("Наличие беременности")
            pregnancy.move(130,150)
            pregnancy.adjustSize()
            pregnancy_list.show() # show combo box

        elif str(gender_list.currentText()) == "M":
            pregnancy.setText(" ")   
            pregnancy_list.hide() 
            gestational_age_line.hide()
            gestational_age.hide()

    def onActivated_pregnancy_list():
        if str(pregnancy_list.currentText()) == "Да":
            gestational_age.show()
            gestational_age_line.show()
        else:
            gestational_age.hide()
            gestational_age_line.hide()

    # вызывает выпадающий список пола Мужской или Женский
    gender_list.activated[str].connect(onActivated_gender_list) 
    # вызывает выпадающий список наличия беременности
    pregnancy_list.activated[str].connect(onActivated_pregnancy_list) 

    # адрес по паспорту
    adres = QLabel(window)
    adres.setText("Адрес по паспорту")
    adres.move(5,213)
    adres.adjustSize()

    adres_list = QComboBox(window)
    adres_list.addItem("Город")
    adres_list.addItem("Село")
    adres_list.addItem("Иногородний РФ")
    adres_list.addItem("Иногородний иностранец")
    adres_list.addItem("БОМЖ")
    adres_list.adjustSize()
    adres_list.move(0, 230)

    adres_full = QLabel(window)
    adres_full.setText("Введите адрес полностью")
    adres_full.move(200,213)
    adres_full.adjustSize()

    adres_line = QLineEdit(window)
    adres_line.move(200,230) 
    adres_line.resize(300,30)   

    # Диагноз по МКБ -10
    diagnos = QLabel(window)
    diagnos.setText("Диагноз по МКБ -10")
    diagnos.move(5,260)
    diagnos.adjustSize()

    list_diagnos_mkb = ["A50.0","A50.1","A50.2","A50.3","A50.4","A50.5",
    "A50.6","A50.7","A50.8","A50.9","A51.0","A51.1","A51.2","A51.3",
    "A51.4","A51.5","A51.9","A52.0","A52.1","A52.2","A52.3","A52.7","A52.8","A52.9",
    "A53.0","A53.9"]
    diagnos_list = QComboBox(window)
    diagnos_list.addItems(list_diagnos_mkb)
    diagnos_list.adjustSize()
    diagnos_list.move(0, 275)

    # Дата установления диагноза
    date_diagnos = QLabel(window)
    date_diagnos.setText("Дата установления диагноза\nпример ввода: 25.07.1900")
    date_diagnos.move(5,305)
    date_diagnos.adjustSize()

    date_diagnos_line = QLineEdit(window)
    date_diagnos_line.move(0,335)

    # Обстоятельства выявления
    find_diagnos = QLabel(window)
    find_diagnos.setText("Обстоятельства выявления")
    find_diagnos.move(5,370)
    find_diagnos.adjustSize()

    find_diagnos_list = QComboBox(window)
    find_diagnos_list.addItem("сам")
    find_diagnos_list.addItem("по контакту")
    find_diagnos_list.addItem("по поручению")
    find_diagnos_list.addItem("дерматологом")
    find_diagnos_list.addItem("дерматологом (интеркурентное заболевание)")
    find_diagnos_list.addItem("при медицинском осмотре,очередной м/о")
    find_diagnos_list.addItem("при поступление на работу")
    find_diagnos_list.addItem("при обследование для ФМС")
    find_diagnos_list.addItem("донор")
    find_diagnos_list.addItem("прочие мед.осмотры")
    find_diagnos_list.addItem("поликлинические учереждения")
    find_diagnos_list.addItem("стационарные учереждения")
    find_diagnos_list.adjustSize()
    find_diagnos_list.move(0, 385)

    # Введите профиль стационара
    policlinic = QLabel(window)
    policlinic_line = QLineEdit(window)
    policlinic_line.setText("")
    policlinic_line.move(275,385)
    policlinic_line.resize(150,20)
    policlinic_line.hide()
    # Линия для ввода специальности врача
    doc_line = QLineEdit(window)
    doc_line.setText("")
    doc_line.move(275,385)
    doc_line.resize(150,20)
    doc_line.hide()



    def onActivated_diagnos():
        if str(find_diagnos_list.currentText()) == "поликлинические учереждения":
            policlinic.setText("Введите профиль стационара")
            policlinic.move(275,370)
            policlinic.adjustSize()
            policlinic_line.show()
            doc_line.hide()

        elif str(find_diagnos_list.currentText()) == "стационарные учереждения":
            policlinic.setText("Введите специальность врача")
            policlinic.move(275,370)
            policlinic.adjustSize()
            policlinic_line.hide()
            doc_line.show()
        else:
            policlinic.setText("")
            policlinic_line.hide()
            doc_line.hide()

    find_diagnos_list.activated[str].connect(onActivated_diagnos) 

    # лабораторные показатели при взятие на учет
    lab_test = QLabel(window)
    lab_test.setText("Лабораторные показатели при взятие на учет")
    lab_test.move(5,415)
    lab_test.adjustSize()

    button_RMP = QRadioButton("РМП",window)
    button_RMP.move(5,425)
    button_RMP.setChecked(False)
    RMP_index_line = QLineEdit(window)
    RMP_index_line.setText("")
    RMP_index_line.move(250,425)
    RMP_index_line.hide()

    button_IFA_SUM = QRadioButton("ИФА Суммарный",window)
    button_IFA_SUM.move(5,445)
    button_IFA_SUM.setChecked(False)
    IFA_SUM_index_line = QLineEdit(window)
    IFA_SUM_index_line.setText("")
    IFA_SUM_index_line.move(250,445)
    IFA_SUM_index_line.hide()

    button_IFA_JgM = QRadioButton("ИФА Jg M",window)
    button_IFA_JgM.move(5,465)
    button_IFA_JgM.setChecked(False)
    IFA_JgM_index_line = QLineEdit(window)
    IFA_JgM_index_line.setText("")
    IFA_JgM_index_line.move(250,465)
    IFA_JgM_index_line.hide()

    button_IFA_JgG = QRadioButton("ИФА Jg G",window)
    button_IFA_JgG.move(5,485)
    button_IFA_JgG.setChecked(False)
    IFA_JgG_index_line = QLineEdit(window)
    IFA_JgG_index_line.setText("")
    IFA_JgG_index_line.move(250,485)
    IFA_JgG_index_line.hide()

    button_RPGA = QRadioButton("РПГА",window)
    button_RPGA.move(5,505)
    button_RPGA.setChecked(False)
    RPGA_index_line = QLineEdit(window)
    RPGA_index_line.setText("")
    RPGA_index_line.move(250,505)
    RPGA_index_line.hide()

    button_RIF = QRadioButton("РИФ",window)
    button_RIF.move(5,525)
    button_RIF.setChecked(False)
    RIF_index_line = QLineEdit(window)
    RIF_index_line.setText("")
    RIF_index_line.move(250,525)
    RIF_index_line.hide()

    button_IB = QRadioButton("ИБ",window)
    button_IB.move(5,545)
    button_IB.setChecked(False)
    IB_index_line = QLineEdit(window)
    IB_index_line.setText("")
    IB_index_line.move(250,545)
    IB_index_line.hide()

    button_Likvor = QRadioButton("Ликвор",window)
    button_Likvor.move(5,565)
    button_Likvor.setChecked(False)
    Likvor_index_line = QLineEdit(window)
    Likvor_index_line.setText("")
    Likvor_index_line.move(250,565)
    Likvor_index_line.hide()


    def btnRMP():
        button_RMP = window.sender()
        if button_RMP.isChecked() == True:
            RMP_index_line.show()
        elif button_RMP.isChecked() == False and RMP_index_line.text() != "":
            RMP_index_line.show()
        else:
            RMP_index_line.hide()
    def btnIfaSum():
        button_IFA_SUM = window.sender()
        if button_IFA_SUM.isChecked() == True:
            IFA_SUM_index_line.show()
        elif button_IFA_SUM.isChecked() == False and IFA_SUM_index_line.text() != "":
            IFA_SUM_index_line.show()
        else:
            IFA_SUM_index_line.hide()
    def btnIfaJgm():
        button_IFA_JgM = window.sender()
        if button_IFA_JgM.isChecked() == True:
            IFA_JgM_index_line.show()
        elif button_IFA_JgM.isChecked() == False and IFA_JgM_index_line.text() != "":
            IFA_JgM_index_line.show()
        else:
            IFA_JgM_index_line.hide()

    def btnIfaJgg():
        button_IFA_JgG = window.sender()
        if button_IFA_JgG.isChecked() == True:
            IFA_JgG_index_line.show()
        elif button_IFA_JgG.isChecked() == False and IFA_JgG_index_line.text() != "":
            IFA_JgG_index_line.show()
        else:
            IFA_JgG_index_line.hide()

    def RPGA():
        button_RPGA = window.sender()
        if button_RPGA.isChecked() == True:
            RPGA_index_line.show()
        elif button_RPGA.isChecked() == False and RPGA_index_line.text() != "":
            RPGA_index_line.show()
        else:
            RPGA_index_line.hide()

    def RIF():
        button_RIF = window.sender()
        if button_RIF.isChecked() == True:
            RIF_index_line.show()
        elif button_RIF.isChecked() == False and RIF_index_line.text() != "":
            RIF_index_line.show()
        else:
            RIF_index_line.hide()

    def IB():
        button_IB = window.sender()
        if button_IB.isChecked() == True:
            IB_index_line.show()
        elif button_IB.isChecked() == False and IB_index_line.text() != "":
            IB_index_line.show()
        else:
            IB_index_line.hide()

    def Likvor():
        button_Likvor = window.sender()
        if button_Likvor.isChecked() == True:
            Likvor_index_line.show()
        elif button_Likvor.isChecked() == False and Likvor_index_line.text() != "":
            Likvor_index_line.show()
        else:
            Likvor_index_line.hide()
   
    accompanying_illnesses = QLabel(window)
    accompanying_illnesses.setText("Сопутствующие заболевания")
    accompanying_illnesses.move(530,25)
    accompanying_illnesses.adjustSize()

    accompanying_illnesses_line = QLineEdit(window)
    accompanying_illnesses_line.move(530,45)
    accompanying_illnesses_line.resize(330,30)
 
    main_course = QLabel(window)
    main_course.setText("Основной курс лечения")
    main_course.move(530,75)
    main_course.adjustSize()

    main_course_line = QLineEdit(window)
    main_course_line.move(530,90)
    main_course_line.resize(330,30)

    additional_course = QLabel(window)
    additional_course.setText("Дополнительный курс лечения")
    additional_course.move(530,120)
    additional_course.adjustSize()

    dditional_course_line = QLineEdit(window)
    dditional_course_line.move(530,135)
    dditional_course_line.resize(330,30)
    
    

    # вызывает функцию btnstate и открывает или закрывает поле ввода, для показателей анализа
    button_RMP.toggled.connect(btnRMP)
    button_IFA_SUM.toggled.connect(btnIfaSum)
    button_IFA_JgM.toggled.connect(btnIfaJgm)
    button_IFA_JgG.toggled.connect(btnIfaJgg)
    button_RPGA.toggled.connect(RPGA)
    button_RIF.toggled.connect(RIF)
    button_IB.toggled.connect(IB)
    button_Likvor.toggled.connect(Likvor)

    # Кнопка для сохранения данных
    button = QPushButton(window)
    button.move(5,595)
    button.setText("Сохранить")




    def on_button_clicked():
        global df
        df = pd.read_excel("C:\Python/softdoc/tablica.xlsx")

        # Высчитывание возраста пациента
        a = date_birth_line.text()
        b = date_diagnos_line.text()

        date1 = datetime.strptime(str(a),"%d.%m.%Y")
        date2 = datetime.strptime(str(b),"%d.%m.%Y")

        delta = relativedelta.relativedelta(date2, date1).years# функция высчитывающая разницу дат в годах.years   месяцах.months и днях.days

        # определения диапазона возраста
        age_range = ''
        if delta<=1:
            age_range = '0-1'
        elif 2 <= delta <=14:
            age_range = '2-14'
        elif 15 <= delta <=17:
            age_range = '15-17'
        elif 18 <= delta <=29:
            age_range = '18-29'
        elif 30 <= delta <=39:
            age_range = '30-39'
        else:
            age_range = '40+'


        new_sting = {'номер пациента':len(df)+1,
                    "фио":FIO_line.text(),
                    "дата рождения":date_birth_line.text(),
                    "пол":str(gender_list.currentText()),
                    "Наличие беременности": str(pregnancy_list.currentText()) ,
                    "Срок беременности": gestational_age_line.text(),
                    "адрес по паспорту":str(adres_list.currentText()),
                    "Диагноз по МКБ-10":str(diagnos_list.currentText()),
                    "Дата установления диагноза":date_diagnos_line.text(),
                    "Обстоятельства выявления":str(find_diagnos_list.currentText()),
                    "Профиль стационара": policlinic_line.text(),
                    "Специальность врача": doc_line.text(),
                    "РМП при взятии":RMP_index_line.text(),
                    "ИФА Суммарный при взятии":IFA_SUM_index_line.text(),
                    "ИФА Jg M при взятии":IFA_JgM_index_line.text(),
                    "ИФА Jg G при взятии":IFA_JgG_index_line.text(),
                    "РПГА при взятии":RPGA_index_line.text(),
                    "РИФ при взятии":RIF_index_line.text(),
                    "ИБ при взятии":IB_index_line.text(),
                    "Ликвор при взятии":Likvor_index_line.text(),
                    "Сопутствующие заболевания":accompanying_illnesses_line.text(),
                    "Основной курс лечения":main_course_line.text(),
                    "Дополнительный курс лечения":dditional_course_line.text(),
                    "Возраст":delta,
                    "диапазон возраста": age_range
                    }
        df =  df._append(new_sting, ignore_index=True)
        df.to_excel('C:\Python/softdoc/tablica.xlsx',index=False)

        print(df)

        alert = QMessageBox()
        alert.setText('Данные сохранены')
        alert.exec()

    button.clicked.connect(on_button_clicked)

    # кнопка для поиска или редактирования пациента
    button_found = QPushButton(window)
    button_found.move(150,595)
    button_found.setText("найти")

    # кнопка для снятия с учета
    button_edit = QPushButton(window)
    button_edit.move(250,595)
    button_edit.setText("Снять с учета")\
    


    class AnotherWindow(QWidget):  # класс для создания новых окон
        def __init__(self):
            super().__init__()
            layout = QVBoxLayout()
            self.label = QLabel("")
            layout.addWidget(self.label)
            self.setLayout(layout)

    def find_patient_window(): # кнопка переход на окно по поиску пациентов
        global window_find

        window_find = AnotherWindow() 
        window_find.setGeometry(150,150,670,350)        
        
        find = QLabel(window_find)
        find.setText("Введите фамилию, имя и отчество пациента")
        find.move(0,0)
        find.adjustSize()

        fio_line = QLineEdit(window_find)
        fio_line.setText("")
        fio_line.move(0,25)
        fio_line.resize(200,25)

        patient_button_found = QPushButton(window_find)
        patient_button_found.move(202,25)
        patient_button_found.setText("Найти пациента")

        save_button_edit = QPushButton(window_find)
        save_button_edit.move(312,25)
        save_button_edit.setText("Сохранить\n изменения")
  
        table = QTableWidget(window_find)  # Создаем таблицу
        table.setColumnCount(34)     #Количество колонок       
        table.setHorizontalHeaderLabels(["Номер пациента", "ФИО", "дата рождения", "пол", "Наличие беременности", 
        "Срок беременности","Адрес по паспорту", "Адрес полностью", "Диагноз по МКБ-10", "Дата установления диагноза", 
        "Обстоятельства выявления","Профиль стационара","Специальность врача", "РМП при взятии", "ИФА Суммарный при взятии", "ИФА Jg M при взятии",
        "ИФА Jg G при взятии","РПГА при взятии", "РИФ при взятии", "ИБ при взятии", "Ликвор при взятии","Сопутствующие заболевания",
        "Основоной курс лечения","Дополнительный курс лечения", "Дата снятия с учета", "Причины снятия с учета", "РМП при снятии",
        "ИФА Суммарный при снятии","ИФА JgM при снятии","ИФА JgG при снятии",  "РПГА при снятии",
        "РИФ при снятии","ИБ при снятии",  "Ликвор при снятии"  ])
        table.resize(650,280)
        table.move(0,60)
        table.resizeColumnsToContents()

        def find_patient_button():  # кнопка поиск пациентов
            
            fio_line_find = fio_line.text() # переменная для сравнения введенного имени и имени в таблице
            output_tab = pd.DataFrame(df.loc[df['фио'] == fio_line_find])
            table.setRowCount(len(output_tab)) #Количество строк

            for i in range(len(output_tab)):
                ser = output_tab.iloc[i].reset_index(drop=True).squeeze() # из DataFrame в DataSeries

                for j in range(34):
                    table.setItem(i, j, QTableWidgetItem(str(ser[j])))
            
        def save_edit_button():
            global df

            row = table.rowCount()
            column = table.columnCount()

            for i in range(row):# кол-во строк в выведеной таблице 
                for j in range(column): #кол-во столбцов в выведеной таблице у нас это константа 34
                    df.iloc[[int(float(table.item(i,0).text()))],[j]] = str(table.item(i,j).text())
                    
            df.to_excel('C:\Python/softdoc/tablica.xlsx',index=False)
            
            alert = QMessageBox()
            alert.setText('Данные сохранены')
            alert.exec()

        patient_button_found.clicked.connect(find_patient_button)
        save_button_edit.clicked.connect(save_edit_button)
        window_find.show()

    button_found.clicked.connect(find_patient_window)

    def edit_patient_window():# объявление переменных в окне снять с учета

        global window_edit

        window_edit = AnotherWindow()
        window_edit.setGeometry(600,200,500,600)        
        
        find_edit = QLabel(window_edit)
        find_edit.setText("Введите номер пациента для редактирования")
        find_edit.move(0,0)
        find_edit.adjustSize()

        find_line_edit = QLineEdit(window_edit)
        find_line_edit.setText("")
        find_line_edit.move(0,25)

        date_end = QLabel(window_edit)
        date_end.setText("Дата снятия с учета")
        date_end.move(0,60)
        date_end.adjustSize()
        
        date_end_line = QLineEdit(window_edit)
        date_end_line.setText("")
        date_end_line.move(0,75)

        reasons_withdrawal = QLabel(window_edit)
        reasons_withdrawal.setText("Причины снятия с учета")
        reasons_withdrawal.move(0,110)
        reasons_withdrawal.adjustSize()

        reasons_withdrawal_list = QComboBox(window_edit)
        reasons_withdrawal_list.addItem("по окончанию срока наблюдения")
        reasons_withdrawal_list.addItem("В связи с реинфекцией")
        reasons_withdrawal_list.addItem("как неразысканный (закончил лечение)")
        reasons_withdrawal_list.addItem("как неразысканный (не закончил лечение)")
        reasons_withdrawal_list.addItem("переведен в другое лечебное заведение")
        reasons_withdrawal_list.addItem("умерший от сифилиса")
        reasons_withdrawal_list.addItem("умерший новорожденный")
        reasons_withdrawal_list.adjustSize()
        reasons_withdrawal_list.move(0, 125)

        medical_institution = QLabel(window_edit)
        medical_institution.setText("Указать в какое мед.учереждение переведен пациент")
        medical_institution.move(250,110)
        medical_institution.adjustSize()
        medical_institution.hide()

        medical_institution_line = QLineEdit(window_edit)
        medical_institution_line.setText("")
        medical_institution_line.move(250,125)
        medical_institution_line.hide()
        medical_institution_line.resize(140,30)

        def onActivated_medical_institution():
            if str(reasons_withdrawal_list.currentText()) == "переведен в другое лечебное заведение":
                medical_institution.show()
                medical_institution_line.show()
            else:
                medical_institution_line.hide()
                medical_institution.hide()
                
        reasons_withdrawal_list.activated[str].connect(onActivated_medical_institution) 

        lab_test = QLabel(window_edit)
        lab_test.setText("Лабораторные показатели при снятие с учета")
        lab_test.move(5,185)
        lab_test.adjustSize()

        button_RMP_edit = QRadioButton("РМП",window_edit)
        button_RMP_edit.move(5,205)
        button_RMP_edit.setChecked(False)
        RMP_index_line_edit = QLineEdit(window_edit)
        RMP_index_line_edit.setText("")
        RMP_index_line_edit.move(250,205)
        RMP_index_line_edit.hide()

        button_IFA_SUM_edit = QRadioButton("ИФА Суммарный",window_edit)
        button_IFA_SUM_edit.move(5,230)
        button_IFA_SUM_edit.setChecked(False)
        IFA_SUM_index_line_edit = QLineEdit(window_edit)
        IFA_SUM_index_line_edit.setText("")
        IFA_SUM_index_line_edit.move(250,230)
        IFA_SUM_index_line_edit.hide()


        button_IFA_JgM_edit = QRadioButton("ИФА JgM",window_edit)
        button_IFA_JgM_edit.move(5,255)
        button_IFA_JgM_edit.setChecked(False)
        IFA_JgM_index_line_edit = QLineEdit(window_edit)
        IFA_JgM_index_line_edit.setText("")
        IFA_JgM_index_line_edit.move(250,255)
        IFA_JgM_index_line_edit.hide()

        button_IFA_JgG_edit = QRadioButton("ИФА JgG",window_edit)
        button_IFA_JgG_edit.move(5,280)
        button_IFA_JgG_edit.setChecked(False)
        IFA_JgG_index_line_edit = QLineEdit(window_edit)
        IFA_JgG_index_line_edit.setText("")
        IFA_JgG_index_line_edit.move(250,280)
        IFA_JgG_index_line_edit.hide()

        button_RPGA_edit = QRadioButton("РПГА",window_edit)
        button_RPGA_edit.move(5,305)
        button_RPGA_edit.setChecked(False)
        RPGA_index_line_edit = QLineEdit(window_edit)
        RPGA_index_line_edit.setText("")
        RPGA_index_line_edit.move(250,305)
        RPGA_index_line_edit.hide()

        button_RIF_edit = QRadioButton("РИФ",window_edit)
        button_RIF_edit.move(5,330)
        button_RIF_edit.setChecked(False)
        RIF_index_line_edit = QLineEdit(window_edit)
        RIF_index_line_edit.setText("")
        RIF_index_line_edit.move(250,330)
        RIF_index_line_edit.hide()

        button_IB_edit = QRadioButton("ИБ",window_edit)
        button_IB_edit.move(5,355)
        button_IB_edit.setChecked(False)
        IB_index_line_edit = QLineEdit(window_edit)
        IB_index_line_edit.setText("")
        IB_index_line_edit.move(250,355)
        IB_index_line_edit.hide()

        button_Likvor_edit = QRadioButton("Ликвор",window_edit)
        button_Likvor_edit.move(5,380)
        button_Likvor_edit.setChecked(False)
        Likvor_index_line_edit = QLineEdit(window_edit)
        Likvor_index_line_edit.setText("")
        Likvor_index_line_edit.move(250,380)
        Likvor_index_line_edit.hide()

        def btnRMP_edit():
            button_RMP_edit = window_edit.sender()
            if button_RMP_edit.isChecked() == True:
                RMP_index_line_edit.show()
            elif button_RMP_edit.isChecked() == False and RMP_index_line.text() != "":
                RMP_index_line_edit.show()
            else:
                RMP_index_line_edit.hide()
        def btnIfaSum_edit():
            button_IFA_SUM_edit = window_edit.sender()
            if button_IFA_SUM_edit.isChecked() == True:
                IFA_SUM_index_line_edit.show()
            elif button_IFA_SUM_edit.isChecked() == False and IFA_SUM_index_line_edit.text() != "":
                IFA_SUM_index_line_edit.show()
            else:
                IFA_SUM_index_line_edit.hide()
        def btnIfaJgm_edit():
            button_IFA_JgM_edit = window_edit.sender()
            if button_IFA_JgM_edit.isChecked() == True:
                IFA_JgM_index_line_edit.show()
            elif button_IFA_JgM_edit.isChecked() == False and IFA_JgM_index_line_edit.text() != "":
                IFA_JgM_index_line_edit.show()
            else:
                IFA_JgM_index_line_edit.hide()

        def btnIfaJgg_edit():
            button_IFA_JgG_edit = window_edit.sender()
            if button_IFA_JgG_edit.isChecked() == True:
                IFA_JgG_index_line_edit.show()
            elif button_IFA_JgG_edit.isChecked() == False and IFA_JgG_index_line_edit.text() != "":
                IFA_JgG_index_line_edit.show()
            else:
                IFA_JgG_index_line_edit.hide()

        def RPGA_edit():
            button_RPGA_edit = window_edit.sender()
            if button_RPGA_edit.isChecked() == True:
                RPGA_index_line_edit.show()
            elif button_RPGA_edit.isChecked() == False and RPGA_index_line_edit.text() != "":
                RPGA_index_line_edit.show()
            else:
                RPGA_index_line_edit.hide()

        def RIF_edit():
            button_RIF_edit = window_edit.sender()
            if button_RIF_edit.isChecked() == True:
                RIF_index_line_edit.show()
            elif button_RIF_edit.isChecked() == False and RIF_index_line_edit.text() != "":
                RIF_index_line_edit.show()
            else:
                RIF_index_line_edit.hide()

        def IB_edit():
            button_IB_edit = window_edit.sender()
            if button_IB_edit.isChecked() == True:
                IB_index_line_edit.show()
            elif button_IB_edit.isChecked() == False and IB_index_line_edit.text() != "":
                IB_index_line_edit.show()
            else:
                IB_index_line_edit.hide()

        def Likvor_edit():
            button_Likvor_edit = window_edit.sender()
            if button_Likvor_edit.isChecked() == True:
                Likvor_index_line_edit.show()
            elif button_Likvor_edit.isChecked() == False and Likvor_index_line_edit.text() != "":
                Likvor_index_line_edit.show()
            else:
                Likvor_index_line_edit.hide()

        button_edit_save = QPushButton(window_edit) # Кнопка 
        button_edit_save.move(5,430)
        button_edit_save.setText("Сохранить")

        def save_button_edit():
            global df
            df = pd.read_excel("C:\Python/softdoc/tablica.xlsx")
            df.loc[int(find_line_edit.text()),'Дата снятия с учета'] = date_end_line.text()
            df.loc[int(find_line_edit.text()),'Причины снятия с учета'] = str(reasons_withdrawal_list.currentText())
            df.loc[int(find_line_edit.text()),'РМП при снятии'] = RMP_index_line_edit.text()
            df.loc[int(find_line_edit.text()),'ИФА Суммарный при снятии'] = IFA_SUM_index_line_edit.text()
            df.loc[int(find_line_edit.text()),'ИФА Jg M при снятии'] = IFA_JgM_index_line_edit.text()
            df.loc[int(find_line_edit.text()),'ИФА Jg G при снятии'] = IFA_JgG_index_line_edit.text()
            df.loc[int(find_line_edit.text()),'РПГА при снятии'] = RPGA_index_line_edit.text()
            df.loc[int(find_line_edit.text()),'РИФ при снятии'] = RIF_index_line_edit.text()
            df.loc[int(find_line_edit.text()),'ИБ при снятии'] = IB_index_line_edit.text()
            df.loc[int(find_line_edit.text()),'Ликвор при снятии'] = Likvor_index_line_edit.text()
            df.to_excel('C:\Python/softdoc/tablica.xlsx',index=False)

            print(df)

            alert = QMessageBox()
            alert.setText('Данные сохранены')
            alert.exec()

        button_edit_save.clicked.connect(save_button_edit)

        button_RMP_edit.toggled.connect(btnRMP_edit)
        button_IFA_SUM_edit.toggled.connect(btnIfaSum_edit)
        button_IFA_JgM_edit.toggled.connect(btnIfaJgm_edit)
        button_IFA_JgG_edit.toggled.connect(btnIfaJgg_edit)
        button_RPGA_edit.toggled.connect(RPGA_edit)
        button_RIF_edit.toggled.connect(RIF_edit)
        button_IB_edit.toggled.connect(IB_edit)
        button_Likvor_edit.toggled.connect(Likvor_edit)    
        window_edit.show()
    button_edit.clicked.connect(edit_patient_window)

    #кнопка для формирования отчета
    button_report = QPushButton(window)
    button_report.move(350,595)
    button_report.setText("Сформировать\n отчет")
    
    
    def report():# объявление переменных в окне сформировать отчет

        global report_window
        report_window = AnotherWindow()
        report_window.setGeometry(600,200,500,600) 

        date_report = QLabel(report_window)
        date_report.setText("""Введите диапазон дат по которым хотите сформировать отчет 
например: 23.06.2001-23.07.2001\n 
Дата начала                     Дата конца""")
        date_report.move(0,10)
        date_report.adjustSize()
        
        date_start_line = QLineEdit(report_window)
        date_start_line.setText("")
        date_start_line.move(0,70)
        date_start_line.resize(120,30)

        date_end_line = QLineEdit(report_window)
        date_end_line.setText("")
        date_end_line.move(130,70)
        date_end_line.resize(120,30)

        button_report_part = QPushButton(report_window)
        button_report_part.move(260,70)
        button_report_part.setText("Сформировать\n отчет")

        button_report_year = QPushButton(report_window)
        button_report_year.move(0,120)
        button_report_year.setText("Сформировать\n отчет за год c декабря")
        button_report_year.adjustSize()

        def generate_report():# функция для формирования выборочного отчета
            global df
            df = pd.read_excel("C:\Python/softdoc/tablica.xlsx")
            # отбор нужных колонок
            df_report = df[["номер пациента","Дата установления диагноза","Диагноз по МКБ-10","фио","пол","дата рождения","Возраст","Адрес полностью","адрес по паспорту","Обстоятельства выявления",
            "РМП при взятии","ИФА Суммарный при взятии","ИФА Jg M при взятии","ИФА Jg G при взятии","РПГА при взятии","РИФ при взятии","ИБ при взятии","Ликвор при взятии"]]
            # создаем новую колонку
            df_report['Лабораторные подтверждения'] = pd.Series()

            list1 = list(df['Дата установления диагноза'])# список всех дат в df
            list_date = [] # список для заненсения индексов входящих в нужный диапазон

            a = date_start_line.text() # получение даты из мтроки
            b = date_end_line.text()
            
            # перевод даты в нужный формат и фильтрация
            for i in range(len(list1)):
                if datetime.strptime(str(a),"%d.%m.%Y") <= datetime.strptime(str(list1[i]),"%d.%m.%Y") <= datetime.strptime(str(b),"%d.%m.%Y"):
                    list_date.append(i)
            print(df_report.iloc[list_date]) # отборнужных строк по индексам

            # функция добавляет анализы если их брали в колонку - 'Лабораторные подтверждения'
            def laboratory_confirmation(column,diagnos):
                list1 = list(df_report[column].isnull())
                
                for i in range(len(list1)):
                    if list1[i] == False:
                        a = str(df_report['Лабораторные подтверждения'][i])
                        a = a + diagnos
                        df_report['Лабораторные подтверждения'][i] = a

            laboratory_confirmation('РМП при взятии',',РМП')
            laboratory_confirmation("ИФА Суммарный при взятии",",ИФА Сумм")
            laboratory_confirmation("ИФА Jg M при взятии",",ИФА Jg M")
            laboratory_confirmation("ИФА Jg G при взятии",",ИФА Jg G")
            laboratory_confirmation("РПГА при взятии",",РПГА")
            laboratory_confirmation("РИФ при взятии",",РИФ")
            laboratory_confirmation("ИБ при взятии",",ИБ")
            laboratory_confirmation("Ликвор при взятии",",Ликвор")

            df_report = df_report.drop(df_report.columns[[10, 11, 12, 13, 14, 15, 16, 17]], axis=1) # удаляем колонки

            df_report['Лабораторные подтверждения'] = df_report['Лабораторные подтверждения'].str[4:] # удаляем первыве 4 символа в строке "nan,"

            df_report.to_excel(f'Сведения о больных за период c {str(a)} по {str(b)}.xlsx',index=False)# состовляем отчет и сохраняем по пути 

        def year_report():
            df = pd.read_excel("C:\Python/softdoc/tablica.xlsx")
            
            # Сводная таблица
            df1 = pd.pivot_table(df,
                        index=["Диагноз по МКБ-10", "пол"],
                        columns=['диапазон возраста'],
                        aggfunc={'диапазон возраста':len},
                        fill_value=0)
            print(df1)
            df2 = df.loc[df['адрес по паспорту'] == "Село"]
            df2 = pd.pivot_table(df2,
                        index=["Диагноз по МКБ-10", "пол",'адрес по паспорту'],
                        columns=['диапазон возраста'],
                        aggfunc={'диапазон возраста':len},
                        fill_value=0)
            print(df2)

            with pd.ExcelWriter('Годовой отчет.xlsx') as writer:  
                df1.to_excel(writer, sheet_name='Sheet_name_1')# состовляем отчет и сохраняем по пути
                try:
                    df2.to_excel(writer, sheet_name='Sheet_name_2')# состовляем отчет и сохраняем по пути
                except IndexError:
                    print("Данных о жителях села нет")

        # отработка функция при нажатие кнопок
        button_report_part.clicked.connect(generate_report)
        button_report_year.clicked.connect(year_report)
        report_window.show()

    button_report.clicked.connect(report)





# Вызов приложения
if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = QMainWindow()
    window.setGeometry(350,350,900,650)# размеры окна
    app.setStyleSheet("QLineEdit{font-size: 15pt;}")

    application()

    window.show()# показ окна
    sys.exit(app.exec_())
